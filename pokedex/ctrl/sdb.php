<?php	

	class SDB extends PDO {
		private static $instance;
		private static $servername = "localhost";
		private static $username = "root";
		private static $password = "";

		public static function GetInstance() {
			if (! self::$instance instanceof self) self::$instance = new SDB();
			return self::$instance;
		}
		
		private function __construct(){
			
			try {
				parent::__construct("mysql:host=".self::$servername.";dbname=pokedex;charset=UTF8", self::$username, self::$password);
				
			
				//$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//echo "Connected successfully"; 
			}
			catch(PDOException $e)
			{
			echo "Connection failed: " . $e->getMessage();
			}	
		}
		
		
	
	}
?>