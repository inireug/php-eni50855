CREATE SCHEMA IF NOT EXISTS pokedex DEFAULT CHARACTER SET utf8 ;
USE pokedex ;

CREATE TABLE IF NOT EXISTS pokedex.pokemon ( 
	id_pokemon INT NOT NULL AUTO_INCREMENT , 
	nom VARCHAR(90) NOT NULL , 
	img VARCHAR(255) NULL , 
	evolution INT(4) NULL , 
	description VARCHAR(255) NULL , 
	fk_type TINYINT(2) NOT NULL,
	PRIMARY KEY (id_pokemon)	
) ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS pokedex.competence ( 
	id_competence INT NOT NULL AUTO_INCREMENT , 
	fk_type INT NOT NULL , 
	libelle VARCHAR(90) NOT NULL , 
	PRIMARY KEY (id_competence)
) ENGINE = InnoDB;
	

CREATE TABLE IF NOT EXISTS pokedex.types (
 id_type INT NOT NULL AUTO_INCREMENT , 
 libelle VARCHAR(90) NOT NULL , 
 PRIMARY KEY (id_type)
) ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS pokedex.pokemon_competence(
    fk_pokemon INT NOT NULL,
    fk_competence INT NOT NULL
) ENGINE = InnoDB;

ALTER TABLE pokedex.pokemon_competence
	ADD FOREIGN KEY (fk_pokemon) REFERENCES pokemon(id_pokemon) ON DELETE CASCADE ON UPDATE CASCADE,
	ADD FOREIGN KEY (fk_competence) REFERENCES competence(id_competence) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE pokedex.competence
	ADD FOREIGN KEY (fk_type) REFERENCES types(id_type) ON DELETE NO ACTION ON UPDATE NO ACTION;
	
ALTER TABLE pokedex.pokemon
	ADD FOREIGN KEY (evolution) REFERENCES pokemon(id_pokemon) ON DELETE CASCADE ON UPDATE CASCADE;

	