<?php
require_once 'ctrl/sdb.php';

$conn = SDB::GetInstance();
$data = $conn->query('SELECT * FROM pokemon');
//var_dump($data);

?>
<html>

<head>
	<title>Pokedex</title>
	<?php include 'partials/head.php'; ?>
</head>

<body>
	<?php include 'partials/header.php'; ?>
	<main>
		<form>
			<label>Nom <select id="nom" name="nom">
					<?php
					//solution avec FETCH
					while ($pok = $data->fetch()) { ?>
						<option value="<?php echo $pok['id_pokemon'] ?>">
							<?php echo $pok['nom'] ?></option>

					<?php }  ?>
				</select></label>

		</form>
		<button class="btn btn-danger" id="del_pkm">Destroy!!!</button>
	</main>
	<script>
		$(() => {
			//function(){} equivaut à ()=>{}
			$('#del_pkm').click((event) => {
				//event.preventDefault();
				let data = {
					id_pokemon: $('#nom').val(),
				}
				//console.log(data);

				$.get({
					url: "?action=del_confirm_pkm", // /?action...
					data: data,
					success: (result) => {
						console.log(result);
						$('main').append('<div class="alert-success">' + JSON.parse(result).msg + '</div>');

					},
					error: (err) => {
						console.log(err);
						$('main').append('<div class="alert-danger">' + err + '</div>');
					}
				});
			})

		});
	</script>
	<?php include 'partials/footer.php'; ?>
</body>

</html>