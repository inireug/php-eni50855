<html>
	<head>
		<title>Pokedex</title>
		<?php include 'partials/head.php' ?>
		<meta charset="utf-8">
	</head>
	<body>	
		<?php include 'partials/header.php' ?>
		<main></main>
		<?php include 'partials/footer.php' ?>
	</body>
</html>