<?php
require('controller.php');

// Exemple de routeur
if (isset($_GET['action'])) {
	$req = $_GET['action'];
	switch ($req){
		case ('') :
			home();
			break;
		case ('/') :
			home();
			break;
		case ('add_pkm') :
			add_pkm();
			break;
		case ('save_pkm') :
			save_pkm();
			break;
			case ('del_pkm') :
			del_pkm();
			break;
		case ('del_confirm_pkm') :
			del_confirm_pkm();
			break;
		 default:
			require __DIR__ . '/views/404.php';
	}
	/*    elseif ($req == 'post') {
        if (isset($_GET['id']) && $_GET['id'] > 0) {
            post();
        }
        else {
            echo 'Erreur : aucun identifiant de billet envoyé';
        }
    }*/
}
else {
	//echo 'param action du GET non renseigné';
    home();
}
