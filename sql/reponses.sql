	-- 1
	SELECT f.titre, f.genre, f.date_sortie, r.nom, r.prenom FROM film f, realisateur r
	WHERE f.ident_realisateur = r.ident_realisateur
	AND r.nationalite = 1
	ORDER BY f.titre;
	
	-- OU
	
	SELECT * FROM film f, realisateur r, pays p
	WHERE f.ident_realisateur = r.ident_realisateur 
	AND r.nationalite = p.ident_pays
	AND p.libelle = 'FRANCE'
	ORDER BY f.titre;
	
	-- OU
	SELECT * FROM film f
	INNER JOIN realisateur r ON f.ident_realisateur = r.ident_realisateur
	INNER JOIN pays p ON r.nationalite = p.ident_pays
	WHERE p.libelle = 'FRANCE'
	ORDER BY f.titre;
	
	
	-- 2
	SELECT f.titre, f.date_sortie, r.nom realisateur, a.nom acteur, a.date_naissance, s.budget
	FROM film f, realisateur r, casting c, acteur a, statistique s
	WHERE f.ident_realisateur = r.ident_realisateur
	AND f.ident_film = c.ident_film
	AND f.ident_film = s.ident_film
	AND c.ident_acteur = a.ident_acteur
	ORDER BY f.titre, a.nom desc;
	
	-- 3
	SELECT f.titre, f.date_sortie, r.nom realisateur, f.distributeur, count(*) as nb_acteurs
	FROM film f, realisateur r, casting c, acteur a
	WHERE f.ident_realisateur = r.ident_realisateur
	AND f.ident_film = c.ident_film
	AND c.ident_acteur = a.ident_acteur
	GROUP BY f.titre 
	ORDER BY f.titre;
	
	-- 4
	SELECT f.titre, f.date_sortie, CONCAT(r.prenom,' ',r.nom) realisateur, CONCAT(a.prenom,' ',a.nom) acteur, a.date_naissance, a.nb_film, s.budget, s.nb_entree_france entrees
	FROM film f, realisateur r, acteur a, statistique s, pays p, casting c
	WHERE f.ident_realisateur = r.ident_realisateur
	AND f.ident_film = c.ident_film
	AND f.ident_film = s.ident_film
	AND c.ident_acteur = a.ident_acteur
	AND a.nationalite = p.ident_pays
	AND p.libelle = 'ALGERIE'
	ORDER BY f.titre;
	
	-- 4 - Correction
	select 
      f.titre, 
      f.date_sortie, 
      r.nom, 
      r.prenom, 
      a.nom, 
      a.prenom, 
      a.date_naissance, 
      s.budget, 
      s.nb_entree_france 
    from film f 
    left join realisateur r 
      on r.ident_realisateur = f.ident_realisateur 
    left join casting c 
      on c.ident_film = f.ident_realisateur 
    left join acteur a 
      on a.ident_acteur = c.ident_acteur 
    left join statistique s 
      on s.ident_film = f.ident_film 
    where 
      exists (
        select 1 
        from film f1 
        inner join casting c1 
          on c1.ident_film = f1.ident_film 
    	inner join acteur a1 
          on a1.ident_acteur = c1.ident_acteur 
    	inner join pays p1 
          on p1.ident_pays = a1.nationalite 
    	where 
          f1.ident_film = f.ident_film 
          and p1.libelle = 'ALGERIE'
    )
	
	-- 5
	SELECT f.titre, s.recette_monde
	FROM film f, statistique s
	WHERE f.ident_film = s.ident_film
	AND s.recette_monde = (SELECT MAX(recette_monde) FROM statistique)
	ORDER BY f.titre;
	-- OU
	SELECT f.titre, s.recette_monde
	FROM film f, statistique s
	WHERE f.ident_film = s.ident_film
	ORDER BY s.recette_monde DESC LIMIT 1;
	
	-- 6
	SELECT CONCAT(a.prenom,' ',a.nom) acteur, f.titre
	FROM film f, casting c, acteur a, realisateur r
	WHERE f.ident_film = c.ident_film
	AND f.ident_realisateur = r.ident_realisateur
	AND c.ident_acteur = a.ident_acteur
	AND a.ident_acteur IN (
		SELECT ident_acteur FROM casting GROUP BY ident_acteur HAVING COUNT(*) = 2
	)
	
	-- OU
	SELECT CONCAT(a.prenom,' ',a.nom) acteur, COUNT(*) nb_films
	FROM casting c, acteur a
	WHERE c.ident_acteur = a.ident_acteur
	GROUP BY acteur
	HAVING nb_films = 2
	
	
	-- 7
	SELECT CONCAT(a.prenom,' ',a.nom) acteur, a.date_naissance
	FROM film f, realisateur r, acteur a, casting c
	
	WHERE 	f.ident_realisateur = r.ident_realisateur
	AND		f.ident_film = c.ident_film
	AND		c.ident_acteur = a.ident_acteur
	AND		acteur = CONCAT(r.prenom,' ',r.nom)
	AND		a.date_naissance = r.date_naissance
	ORDER BY acteur;
	
	-- OU
	SELECT CONCAT(acteur.nom,' ',acteur.prenom,
	acteur.date_naissance "né le"
	FROM realisateur, acteur
	WHERE acteur.nom = realisateur.nom
	-- AND acteur.prenom = realisateur.prenom -- erreur saisie à l'insertion : dany / danny
	AND acteur.date_naissance = realisateur.date_naissance
	ORDER BY CONCAT(acteur.nom,' ',acteur.prenom)
	
	-- 8
	SELECT CONCAT(a.prenom,' ',a.nom) as acteur, a.date_naissance, c.role, p.libelle, f.titre
	FROM film f, casting c, acteur a, pays p
	WHERE f.ident_film = c.ident_film
	AND c.ident_acteur = a.ident_acteur
	AND p.ident_pays = a.nationalite
	AND f.titre LIKE 'S%'
	-- OU SUBSTR(f.titre,1,1) = 'S'
	
	-- 9
	SELECT CONCAT(a.prenom,' ',a.nom) as acteur, a.date_naissance, sum(c.nb_jour_tournage) nb_jours
	FROM casting c, acteur a
	WHERE c.ident_acteur = a.ident_acteur
	AND a.date_naissance BETWEEN '1948-01-01' AND '1978-05-31'
	GROUP BY CONCAT(a.prenom,' ',a.nom)
	
	
	