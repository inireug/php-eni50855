<?php
class Moteur{
    private $puissance;
    
    function __construct($puissance){
        $this->puissance = $puissance;
    }

    public function consommer($distance){
        return $this->puissance*$distance*0.05;
    }

    public function getPuissance(){
        return $this->puissance;
    }
}

?>