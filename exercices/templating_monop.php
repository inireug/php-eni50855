<?php

	//initialisation d'un tableau de résultats
	$results = [0,0,0,0,0,0,0,0,0,0,0,0,0];
	/*
		results
		0 => 0
		1 => 0
		...
		12 => 0
	*/

	// 100 lancers
	for ($i = 0; $i < 1000; $i++){

		// 1000 lancers de 2 dés à 6 faces :
		$de1 = rand(1,6);
		$de2 = rand(1,6);
		$r = $de1+$de2;
		//echo $de1,' + ',$de2,' : ',$r,'<br/>';		
		$results[$r]++; // $r = 7 ==> $results[7]++ <==> $results[7] = $results[7] +1
	
	}
	//print_r($results);
	$flow ='<table>';
	
	for ($i = 2; $i<=12; $i++){
		$flow .= '<tr>';
		
		$flow .= '<td style="border : 1px solid #ccc">'.$i.'</td><td style="border : 1px solid #ccc">'.$results[$i].'</td>';
		
		$flow .= '<td style="border : 1px solid #ccc">'.$results[$i]/1000*100 .' %</td>';
		
		$flow .= '</tr>';
		// r / nb / %
	}
	$flow .='</table>';
	echo $flow;
 ?>