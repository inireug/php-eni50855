<html>
	<head>
		<title>Roll Dice Simulator !</title>
		<!--link rel="stylesheet" href="bootstrap.min.css" /-->
		<!--script src="bootstrap.min.js"></script-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</head>
	<body>
	<header><nav class="nav navbar-dark bg-dark"><p class=" container text-light">Roll Dice</p></nav></header>
	
	<div class="container">
	<?php 
		// X lancers de Y dés à Z faces !!
		$lancers = (isset($_POST['lancers'])) ? $_POST['lancers'] : 100;
		$des = (isset($_POST['des'])) ? $_POST['des'] : 2;
		$faces = (isset($_POST['faces'])) ? $_POST['faces'] : 6;
	
		function print_results($results, $lancers, $des, $faces){

			$flow ='<table class="table table-striped">';			
			for ($a = $des; $a<= $des*$faces; $a++){
				$flow .= '<tr>';
				$flow .= '<td>'.$a.'</td>'; 
				$flow .= '<td>'.$results[$a].'</td>';
				$flow .= '<td>'.$results[$a]/$lancers*100 .' %</td>';				
				$flow .= '</tr>';
			}
			$flow .='</table>';
			return $flow;
		}
		
	?>
	<form action="roll_dice.php" method="POST">
		<label>Lancers <input type="number" name="lancers" min="1" max="" value="<?php echo $lancers ; ?>" /></label>
		<label>Dés <input type="number" name="des" min="1" max="100" step="1" value="<?php echo $des ; ?>" /></label>
		<label>Faces <input type="number" name="faces" min="1" max="100" step="1" value="<?php echo $faces ; ?>" /></label>
		<input class="btn btn-danger" type="submit" value="GO BABY">
		<!--button class="btn btn-success"> :) </button>
		<button class="btn btn-xl btn-warning"> :o </button>
		<button class="btn btn-xl btn-info"> ! </button-->
	</form>
	<?php
		
		//initialisation d'un tableau de résultats :
		$results = [];
		for ($i = 0; $i <= $des*$faces; $i++) $results[$i] = 0;
		
		//lancers
		for ($j = 1; $j <= $lancers ; $j++){
			//initialisation d'un résultat
			$r = 0;
			//des / faces
			for ($k = 0; $k < $des ; $k++) $r += rand(1,$faces);
			// on enregistre qu'on a obtenu ce résultat 1 fois :
			$results[$r]++;
		}
		
		echo print_results($results, $lancers, $des, $faces);
		
	?>
	</div>
	</body>
</html>