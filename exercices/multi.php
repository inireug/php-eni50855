<?php 
	function multiply_table($n){
		if ($n == null) $n = 10;
		$ret = '<table>';
		for ($i = 1; $i <= $n; $i++){
			$ret.= '<tr>';
			for ($j = 1; $j <= $n; $j++){
				$k = $i*$j;
				//echo '<td style="border: 1px solid #ccc;">'.$k.'</td>';
				$ret.= "<td style=\"border: 1px solid #ccc;\">$i x $j = $k</td>";
			}
			$ret.= '</tr>';
		}
		$ret.= '</table>';	
		return $ret;
	}
?>


<html>
	<head>
		<title>Multiply with Darth Vader</title>
	</head>
	<body>
		<form method="GET" action="multi.php">
			<input type="number" name="n" value="10">
			<input type="submit">
		</form>
		<?php 
			if (isset($_GET['n'])) echo multiply_table($_GET['n']);
		?>
		
		<!--form method="POST" action="multi.php">
			<input type="number" name="n" value="10">
			<input type="submit">
		</form-->
		
		<?php 
			// echo multiply_table($_POST['n']);
		?>
	</body>
</html>