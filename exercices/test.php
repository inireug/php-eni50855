<html>
	<head>
		<title>Memo PHP Syntaxe</title>
	</head>
	<body>

		<?php
			//un bloc d'instruction php commence par une balise ouvrante : <?php ou <? et se termine par une balise fermante ? >
			
			//commentaire
			/* 
			commentaire
			multiligne
			*/
			
			
		
			echo '<div class="truc">Syntaxe</div>'; //sans interpretation : '
			echo "<div class=\"truc\">Syntaxe</div>"; //avec interpretation : "
			
			//concaténation : .= , x.= 1 équivaut à x = x + 1;
			$mavar = '<div ';
			$mavar .= 'class=';
			$mavar .=  '"truc">';
			$mavar .= 'contenu concaténé</div>';
			
			echo $mavar;
			
			//il est possible de refermer un bloc php pour insérer du html pur dans le flux :
		?>
			
		<div class="truc">
		pureHTML
		</div>
			
		<?php //ensuite on le rouvre, on a toujours accès aux variables déclarées plus haut
		echo '<div '
		.'class='
		.'"truc">XY</div>';
		
		
		// utiliser un retour chariot en bash :
		echo "darth\r\n
		vader";
		
		// retour à la ligne en HTML :
		echo "<br/>darth<br/>
		vader<br/>";
		
		/* Variables et Types de données */
		
		// déclarer une variable :
		$nom_variable = 'foo'; 			//string
		$var2 = 14;						//int
		$var3 = 14.47;					//float
		$var4 = false;					//bool
		$var5 = [$var2,$var3,$var4];	//array
		$var_vide = null; 				// ou NULL
		
		//echo $var5; //déclenche un avertissement car un tableau n'est pas de type str.
		
		//print_r pour afficher le contenu d'un tableau ou d'un tableau associatif :
		print_r($var5); 
		
		// var_dump pour consulter le contenu d'une variable qui n'est pas de type string :
		var_dump($var5);

		echo "je suis $nom_variable / ";
		//echo 'je suis $nom_variable';
		echo 'je suis '.$nom_variable.' et pivoila';

		//les dates PHP sont de type string
		$var6 = date('l');
		echo '<br/>'.$var6;		
		var_dump($var6);
		
		/* =========== */
		
		/**
			estPair : détermine la parité d'un nombre
			@param n : le nombre à évaluer
			@return chaine de caractères exprimant le résultat
		*/
		
		//définition de fonction
		function estPair($n){
			if ($n%2==0) return "$n est pair";
			else return "$n est impair";
		}
		
		echo '<br/>';
		$nombre = 3;
		//appel de fonction
		echo (estPair($nombre));
		echo '<br/>';
		$nombre = 4;
		echo (estPair($nombre));
		?>

		<h3>Conditions :</h3>
		<?php 
			//if / else if / else classique :
			if ($nombre > 0) echo 'ok'; 
			else if ($nombre == NULL) echo 'valeur nulle';
			else echo 'nook';
			
			//ternaire :
			echo ($nombre > 0) ?  'ok' : 'nook';
			
			//triple égal : égalité en valeur et en TYPE	
			echo '=='.(0 == '0').'<br/>';
			echo '==='.(0 === '0');
		?>

		<h3>Tableaux :</h3>
		<?php 	
			// 3 façons de déclarer des tableaux :
			//$noms = new Array();
			//$noms = array('john','jack','joe');
			$noms = ['Virginie','Ludovic','Sébastien','Aurélien','Béatrice'];
			
			//accéder au contenu de notre tableau :
			echo $noms[0].' '.$noms[1];
			
			//ajouter un élément au tableau :
			$noms[] = 'Jules';
			
			//var_dump($noms);
			
			//tableau associatif :
			
			$tableau_associatif = [
				'a' => 0,
				'b' => 1,
				'c' => true,
				'd' => 3.14712,
				'e' => null
			];
			echo '<br/>';
			print_r($tableau_associatif);
			echo '<br/>';
			$full_story = [
				$noms[0] => 'A',
				$noms[1] => 'B'
			];
			$full_story[$noms[2]] = 'C';
			echo '<br/>';
			//var_dump($full_story);
			//echo ($full_story['Virginie']);
			echo 'Array Keys : <br/>';
			//array_keys pour afficher les clés contenues dans un tableau (asso)
			print_r(array_keys($tableau_associatif));
			
			// on  peut parcourir un tableau classique avec une boucle classique
			for ($i = 0; $i < sizeof($noms); $i++){
				echo $noms[$i].'<br/>';
			} 
			
			// foreach permet de parcourir indifférement un tableau ou un tableau associatif :
			foreach ($tableau_associatif as $element){
				echo $element.'<br/>';
			}
			
		?>
		
	</body>
</html>












